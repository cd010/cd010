//Demonstrate how to read data from the keyboard, write it to a file called INPUT, 
//again read the same data from the INPUT file, and display it on the screen/console.
#include<stdio.h>
int main()
{
	FILE *fp;
	char c;
	printf("Data Input: \n\n");
	fp=fopen("Input2.txt","w");
	while((c=getchar())!=EOF)
	putc(c,fp);
	fclose(fp); 
	printf("Output:");
	fp=fopen("Input2.txt","r");
	while((c=getc(fp))!=EOF)
	printf("%c",c);
	fclose(fp);
	return 0;
}
	
