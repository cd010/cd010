//Develop a C program to enter a number and calculate the sum of its digits.
#include <stdio.h>
#include <math.h>
int main()
{
    int n,a,b;
    printf("Enter a number: ");
    scanf("%d",&n);
    b=0;
    for(int i=0;i<=n;i++)
    {
        a=n%10; 
        n/=10;
        b+=a;
    }
    printf("\n Sum of digits:%d",b);
    return 0;
}
