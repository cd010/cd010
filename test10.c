/*Develop a C program to perform arithmetic operations 
(addition, subtraction, multiplication, division and remainder) on a two integers using pointers.*/
#include<stdio.h>
void sum(int *a,int *b,int *s)
{
    *s=*a+*b;
    
} 
void diff(int *a,int *b,int *d)
{
    *d=*a-*b;
    
}
void mul(int *a,int *b,int *p)
{
    *p=*a * *b;
   
}
void division(int *a,int *b,int *q)
{
    *q=*a / *b;
    

}
void rem(int *a,int *b,int *r)
{
    *r= *a % *b;
    
}
int main()
{
    int a,b,s,d,p,q,r;
    printf("Enter two numbers:");
    scanf("%d %d",&a,&b);
    sum(&a,&b,&s);
    printf("Sum is: %d\n",s);
    diff(&a,&b,&d);
    printf("Difference is: %d\n",d);
    mul(&a,&b,&p);
     printf("Product is: %d\n",p);
    division(&a,&b,&q);
    printf("Quotient is: %d",q);
    rem(&a,&b,&r);
    printf("\nRemainder is: %d",r);
    return 0;
}