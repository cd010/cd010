//Develop a C program to read and print employee details using structures
int main()
{
    struct employee
    {
        int no;
        char name[50],dob[10],cname[50];
        float sal;
    }; 
    struct employee e;
    printf("Employee number:\n");
    scanf("%d",&e.no);
    printf("Employee name:\n");
    scanf("%s",&e.name);
    printf("Date of birth:\n");
    scanf("%s",&e.dob);
    printf("Company name\n");
    scanf("%s",&e.cname);
    printf("Salary:\n");
    scanf("%f",&e.sal);
    
    printf("Employee number:%d\n",e.no);
    printf("Employee name:%s\n",e.name);
    printf("Date of birth:%s\n",e.dob);
    printf("Company name:%s\n",e.cname);
    printf("Salary:%f\n",e.sal);
    return 0;
   
}