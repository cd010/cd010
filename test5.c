//Develop a C program to print multiples of n from 1 to 100
#include <stdio.h>
int main()
{
    int n;
    printf("Enter a number: \n");
    scanf("%d",&n);
    for(int i=1;i<=100;i++)
    {
        if(i%n==0)
        printf("\n %d ",i); 
    }
    return 0;
}