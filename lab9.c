//Illustrate pointers in swapping two numbers.
#include<stdio.h>
void swap( int *a,int *b)
{
    int temp= *a;
    *a=*b;
    *b=temp;
}
 
int main()
{
    int a,b;
    printf("Enter the first number\n");
    scanf("%d",&a);
    printf("Enter the second number\n");
    scanf("%d",&b);
    swap(&a,&b);
    printf("After swapping:\nnum1=%d\nnum2=%d",a,b);
    
    return 0;
    
}