//Write a program to convert a decimal number to a binary number.
#include<stdio.h>
#include<math.h>
int main()
{
    int i,n,a,b;
    printf("Enter a decimal number");
    scanf("%d",&n);
    b=0;
    i=0;
    while(b!=0) 
    {   
        a=n%2;
        b+=a+pow(10,i);
        n%=2;
        i++;
        }
    
    printf("\nBinary equivalent of %d is %d",n,b);
    return 0;
    
}