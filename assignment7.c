/*Given an array of size n=10 which holds odd numbers and array elements as below:
23 27 5 7 33 17 7 19 21 57
Identify the searching technique that can be applied to find the element 21 in the given prime array.
Write a C program to search the given element and print the position of the element.*/
#include <stdio.h>

int main()
{
    int pos;
     int a[10]={23,27,5,7,33,17,7,19,21,57};
    for(int i=0;i<10;i++)
    {
        if(a[i]==21){
        pos=i;
        break;}
    }
    printf("\n21 is present at: %d",pos);
	return 0;
}
