// Interchange the largest and smallest elements in the array
#include<stdio.h>
 
int  main()
{
	int a[50],max,min,maxpos,minpos,i,temp,n;
	
	printf("Number of elements ");
	scanf("%d",&n);
	printf("Enter the elements");
	for(i=0;i<n;i++)
		scanf("%d",&a[i]);
	max=a[0];
	min=a[0];
	maxpos=0;
	minpos=0;
	for(i=1;i<n;i++)
	{
		if(a[i]>max)
		{
			max=a[i];
			maxpos=i;
		}
		if(a[i]<min)
		{
			min=a[i];
			minpos=i;
		}
	}
	temp=a[maxpos];
	a[maxpos]=a[minpos];
	a[minpos]=temp;
	printf("After interchange array elemnts are: ");
	for(i=0;i<n;i++)
		printf("%d ",a[i]);
	return 0;
}