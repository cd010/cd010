/*Develop a C program to create student structure, read two student details
( Student roll number, name, section, department, fees, and results i.e., total marks obtained) 
and print the student details who has scored the highest.*/
#include<stdio.h>
struct stu
{ 
    int rno,fees,res;
    char name[50],s,dept[50];
};
int main()
{
    struct stu s[2]; 
    int i;
    for(i=0;i<2;i++)
    {
    printf("Enter the roll number\n");
    scanf("%d",&s[i].rno);
    printf("Enter the name of student %d\n",i+1); 
    gets(s[i].name);
    printf("Enter the department\n");
    gets(s[i].dept);
    printf("Enter the fees:\n");
    scanf("%d",&s[i].fees);
    printf("Enter the total marks out of 500\n");
    scanf("%d",&s[i].res);
    }
    
    if (s[0].res>s[1].res)
    {
        printf("Roll number:%d\n",s[0].rno);
        printf("Name:%s\n",s[0].name);
        printf("Department:%s\n",s[0].dept);
        printf("Fees:%d\n",s[0].fees);
        printf("Marks:%d\n",s[0].res);
    }
    else
    {
       printf("Roll number:%d\n",s[1].rno);
        printf("Name:%s\n",s[1].name);
        printf("Department:%s\n",s[1].dept);
        printf("Fees:%d\n",s[1].fees);
        printf("Marks:%d",s[1].res); 
    }
    return 0;
}