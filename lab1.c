//Program to compute distance between two points
#include<stdio.h>
#include<math.h>
int main()
{
	float x1,x2,y1,y2;
	float dist;
	printf("Enter the coordinates of point 1: ");
	scanf("%f %f ",&x1,&y1);
	printf("\n Enter the coordinates of point 2: ");
	scanf("%f %f", &x2,&y2);
	float ans;
    ans= (pow((x1-x2),2))+(pow((y1-y2),2)); 
	dist=sqrt(ans);
	printf("Required distance between the two points is: %f",dist);
	return 0;
} 
	//write your code here