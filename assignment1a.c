//1a. Write a program to find the largest of three numbers using ternary operator.
#include<stdio.h>
int main()
{
    int a,b,c,ans;
    printf("enter three numbers: ");
    scanf("%d %d %d",&a,&b,&c);
    ans=(a>b)?((a>c)?a:c):((b>c)?b:c);
    printf("%d is greatest",ans);
    return 0;
}    
    
    