/*Given an array of size n=6 which holds prime number and array elements as below:
2 3 5 7 11 13
Identify the searching technique that can be applied to find the element 11 in the given prime array.
Write a C program to search the given element and print the position of the element.*/
#include<stdio.h>
int main()
{
    int a[6]={2,3,5,7,11,13};
    int i,pos,beg=0,end=5,mid;
   
    mid=(beg+end)/2;
    while(beg<=end)
    {   
        
        if(a[mid]<5)
        beg=mid+1;
        
        else if(a[mid]==5)
        pos=mid;
        
        else
        end=mid-1;
    }    
     
       
    printf("\n5 is present at %d",(pos+1));
    return 0;
    
}