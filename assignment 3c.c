//Develop a C program to find the possible roots of quadratic equation using switch case statement
#include <stdio.h>
#include <math.h>
int main()
{
    int p;
    float a,b,c,r1,r2,d;
    printf("Enter the values of a,b,c: ");
    scanf("%f %f %f",&a,&b,&c);
    d=(b*b)-(4*a*c);
    r1=(-b+sqrt(d))/(2*a);
    r2=(-b-sqrt(d))/(2*a);
    if(d==0)
    p=0;
    else if(d>0)
    p=1;
    else
    p=2;
    switch(p) 
    {
        case 0: 
        printf("\n  Equal roots are : %f,%f",r1,r2);
        break;
        case 1:
        printf(" \n Unequal roots are : %f,%f",r1,r2);
        default:
        printf(" \n imaginary roots");
    }
    return 0;
}
